$historyBody = $('#historyBody');
$progressBody = $('#progressBody');

getMyDocumentGroup().then(function (response) {
    if (response['status'] == 'success') {
        var data = response['value'];
        // console.log(data);
        $historyBody.html('');
        $.each(data['data'], function (i, data) {
            var fileName = data['file'].substr(data['file'].lastIndexOf('/') + 1);
            var row = '';
            row += '<a href="' + data['file'] + '" style="text-decoration: none; color: black;">'
            row += '<div class="d-flex w-max" style="background-color: white; padding: 10px; margin-top: 12px;">';
            row += '<div class="my-auto">';
            row += '<img src="assets/icon/pdf.png">';
            row += '</div>';
            row += '<div class="my-auto ml-3" id="fileName">';
            row += fileName;
            row += '</div>';
            row += '</div>';
            row += '</a>';

            row += '<div data-id=' + data['id'] + ' class="history-title mt-3 pointer" style="width: max-content;">';
            row += 'History <i class="fa fa-chevron-down"></i>';
            row += '</div>';
            row += '<div class="history-content' + data['id'] + ' hide">';

            $.each(data['history'], function (i, historyData) {
                row += '<div class="ml-4 mt-2">';
                row += 'Opened by ' + historyData['name'] + ', ' + historyData['time'];
                row += '</div>';
            });

            row += '</div>';

            $historyBody.append(row);
        });
    }
});

getDocumentToSign().then(function (response) {
    console.log(response);
    if (response['status'] == 'success') {
        var data = response['value'];
        $progressBody.html('');
        $.each(data['data'], function (i, data) {
            var fileName = data['file'].substr(data['file'].lastIndexOf('/') + 1);
            var row = '';
            row += '<div class="d-flex w-max pointer" style="background-color: white; padding: 10px; margin-top: 12px;">';
            row += '<div class="my-auto">';
            row += '<img src="assets/icon/pdf.png">';
            row += '</div>';
            row += '<div class="my-auto ml-3" id="fileName">';
            row += fileName;
            row += '</div>';
            row += '</div>';

            $progressBody.append(row);
        });
    }
});


$('#historyBody').delegate('.btnHistory', 'click', function () {
    if ($(this).children('.history-chevron').hasClass('fa-chevron-up')) {
        $(this).children('.history-chevron').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        $(this).children('.history-list').removeClass('hide');
    } else {
        $(this).children('.history-chevron').addClass('fa-chevron-up').removeClass('fa-chevron-down');
        $(this).children('.history-list').addClass('hide');
    }
});

$('.progress-tab').on('click', function () {
    $('.progress-tab-title').addClass('tab-active');
    $('.history-tab-title').removeClass('tab-active');

    $('.progress-tab').addClass('tab-bar');
    $('.history-tab').removeClass('tab-bar');

    $('.progressing-view').removeClass('hide');
    $('.history-view').addClass('hide');
});

$('.history-tab').on('click', function () {
    $('.history-tab-title').addClass('tab-active');
    $('.progress-tab-title').removeClass('tab-active');

    $('.history-tab').addClass('tab-bar');
    $('.progress-tab').removeClass('tab-bar');

    $('.history-view').removeClass('hide');
    $('.progressing-view').addClass('hide');
});

$('.fa-filter').on('click', function () {
    $('.filter-menu').toggleClass('hide');
});

$('#historyBody').delegate('.history-title', 'click', function () {
    var id = $(this).data('id');
    $('.history-content' + id).toggleClass('hide');
});