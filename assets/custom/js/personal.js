$mydocumentList = $('#myDocumentList');
getMyDocument().then(function (response) {
    console.log(response);
    if (response['status'] == 'success') {
        var data = response['value'];
        $mydocumentList.html('');
        $.each(data['data'], function (i, data) {
            var fileName = data['file'].substr(data['file'].lastIndexOf('/') + 1);
            var row = '';
            row += '<a href="' + data['file'] + '" download style="text-decoration: none; color: black;">';
            row += '<div class="d-flex w-max" style="background-color: white; padding: 10px; margin-top: 12px;">';
            row += '<div class="my-auto">';
            row += '<img src="assets/icon/pdf.png">';
            row += '</div>';
            row += '<div class="my-auto ml-3">';
            row += fileName;
            row += '</div>';
            row += '</div>';
            row += '</a>';
            $mydocumentList.append(row);
        });
    }
});

$('.fa-info-circle').on('click', function () {
    $('.info').toggleClass('hide');
});
