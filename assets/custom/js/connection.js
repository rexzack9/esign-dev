// $url = 'http://52.74.178.166:82';
$url = 'https://codeviable.com/engine-signature/public';
$token = '';
$userId = '';

$token = JSON.parse(localStorage.getItem('eSignToken'));
var user = JSON.parse(localStorage.getItem('eSignUser'));
if (user != null) {
    $userId = user['id'];
}

function login(emailText, passText) {
    var postData = {
        'email': emailText,
        'password': passText,
    };

    return $.ajax({
        url: $url + '/login',
        method: 'POST',
        data: postData,
    }).done(function (response) {
        // console.log(response);
        return response;
    });
}

function signup(nameText, emailText, passText, phoneText) {
    var postData = {
        'name': nameText,
        'email': emailText,
        'password': passText,
        'phone': phoneText,
    };

    return $.ajax({
        url: $url + '/user/register',
        method: 'POST',
        data: postData,
    }).done(function (response) {
        return response;
    });
}

function allUsers() {
    return $.ajax({
        url: $url + '/user/all',
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
    }).done(function (response) {
        return response;
    });
}

function addDocument(postData) {
    return $.ajax({
        url: $url + '/document/add',
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
        method: 'POST',
        data: postData,
        processData: false,
        contentType: false,
    }).done(function (response) {
        return response;
    })
}

function getMyDocument() {
    return $.ajax({
        url: $url + '/mydocument/personal?userid=' + $userId,
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
    }).done(function (response) {
        return response;
    })
}

function getMyDocumentGroup() {
    return $.ajax({
        url: $url + '/mydocument/group?userid=' + $userId,
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
    }).done(function (response) {
        return response;
    })
}

function getDocumentToSign() {
    return $.ajax({
        url: $url + '/receivedocument/user?userid=' + $userId,
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
    }).done(function (response) {
        return response;
    })
}

function addSignature(blob) {
    var formData = new FormData();
    formData.append('userid', $userId);
    // formData.append('signaturebase64', blob, 'sign.png');
    formData.append('signaturefile', blob, 'sign.png');
    return $.ajax({
        url: $url + '/signature/add',
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
        method: 'POST',
        data: formData,
        processData: false,
        contentType: false,
    }).done(function (response) {
        return response;
    });
}

function getSignature() {
    return $.ajax({
        url: $url + '/signature/user?userid=' + $userId,
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
    }).done(function (response) {
        return response;
    });
}

function getPrepareDocs() {
    return $.ajax({
        url: $url + '/document/user?userid=' + $userId,
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
    }).done(function (response) {
        return response;
    });
}

function updateSignedDocuments(docId, file) {
    var formData = new FormData();
    formData.append('docid', docId);
    formData.append('documentfile', file, 'test.pdf');
    return $.ajax({
        url: $url + '/document/successsign',
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
        method: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
    }).done(function (response) {
        return response;
    });
}

function receivedApproved(docId, userId, file) {
    var formData = new FormData();
    formData.append('docid', docId);
    formData.append('userid', userId);
    formData.append('documentfile', file, 'test.pdf');
    return $.ajax({
        url: $url + '/receivedocument/userapprove',
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
        method: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
    }).done(function (response) {
        return response;
    });
}

function receivedReject(docId, userId, file) {
    var formData = new FormData();
    formData.append('docid', docId);
    formData.append('userid', userId);
    formData.append('documentfile', file, 'test.pdf');
    return $.ajax({
        url: $url + '/receivedocument/userrejected',
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
        method: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
    }).done(function (response) {
        return response;
    });
}

function reuploadReject(docId, userId, file) {
    var formData = new FormData();
    formData.append('docid', docId);
    formData.append('userid', userId);
    formData.append('documentfile', file, 'test.pdf');
    return $.ajax({
        url: $url + '/receivedocument/requesterupdate',
        headers: {
            'Authorization': 'Bearer ' + $token,
        },
        method: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
    }).done(function (response) {
        return response;
    });
}

function documentHistory(id) {
    return $.ajax({
        url: $url + '/history/info?docid=' + id,
        headers: {
            'Authorization': 'Bearer ' + $token,
        }
    }).done(function (response) {
        return response;
    });
}