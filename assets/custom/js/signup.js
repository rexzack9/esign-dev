var nameText = '';
var emailText = '';
var passText = '';
var phoneText = '';

$('#createAccount').on('click', function() {
    nameText = $('#nameText').val();
    emailText = $('#emailText').val();
    passwordText = $('#passwordText').val();
    phoneText = $('#phoneText').val();

    if (nameText != '' && emailText != '' && passwordText != '' && phoneText != '') {
        console.log('submit');
        signup(nameText, emailText, passwordText, phoneText).then(function(response) {
            console.log(response);
            if (response['status'] == 'success') {
                login(emailText, passwordText).then(function(response2) {
                    if (response2.status == 'success') {
                        localStorage.setItem('eSignUser', JSON.stringify(response2['value']));
                        localStorage.setItem('eSignToken', JSON.stringify(response2['api_key']));
                        document.cookie = 'eSignLogged=true';
                        window.location.href = 'sign-document.php';
                    } else {
                        $('.login-err').removeClass('hide');
                    }
                });
            }
        });
    }
});