function requestSignature() {
    getSignature().then(function (response) {
        if (response['status'] == 'success') {
            $('#sign-list').html('');
            var signatures = response['value'];
            if (signatures.length != 0) {
                $('#emptyState').addClass('hide');
                $.each(signatures, function (i, data) {
                    $('#sign-list').append('<img class="sign-img mt-2 mr-2" src="' + data['signature'] + '">');
                });
            } else {
                $('#emptyState').removeClass('hide');
            }
        }
    });
}

requestSignature();

$('#sign-canvas').signature();

var signImg = '';

$('#createSignature').on('click', function () {
    $('#sign-box').removeClass('hide');
    $('#sign-canvas').signature({
        background: 'transparent',
        color: 'black',
        syncFormat: 'PNG'
    });
});

$('#cancelSignature').on('click', function () {
    $('#sign-canvas').signature('clear');
    $('#sign-box').addClass('hide');
});

$('#applySignature').on('click', function () {
    $('#tacModal').modal();
});

$('#confirmTac').on('click', function () {
    $('#tacModal').modal('hide');
    signImg = $('#sign-canvas').signature('toDataURL', 'image/png');
    // $('#sign-list').append('<img class="sign-img mt-2 mr-2" src="' + signImg + '">');
    getImage(signImg);
    $('#sign-canvas').signature('clear');
    $('#sign-box').addClass('hide');
});

function getImage(img64) {
    var block = img64.split(';');
    var contentType = block[0].split(':')[1];
    var realData = block[1].split(',')[1];

    var blob = b64toBlob(realData);

    addSignature(blob).then(function (response) {
        if (response['status'] == 'success') {
            requestSignature();
        }
    });
}

const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
}