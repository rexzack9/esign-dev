$.getScript('./assets/custom/js/connection.js', function() {});

$('#navbar').load('navbar/navbar2.html');

$('#mydocumentmenu').on('click', function() {
    if ($('#mydocumentmenubar').hasClass('menu-active')) {
        $('#mydocumentmenubar').removeClass('menu-active');
        $('#mydocumentmenulabel').addClass('inactive-text');
        $('#submenu').addClass('hide');
    } else {
        $('#mydocumentmenubar').addClass('menu-active');
        $('#mydocumentmenulabel').removeClass('inactive-text');
        $('#submenu').removeClass('hide');
    }
});

$('#documentmenu').on('click', function() {
    if ($('#documentmenubar').hasClass('menu-active')) {
        $('#documentmenubar').removeClass('menu-active');
        $('#documentmenulabel').addClass('inactive-text');
        $('#documentsubmenu').addClass('hide');
    } else {
        $('#documentmenubar').addClass('menu-active');
        $('#documentmenulabel').removeClass('inactive-text');
        $('#documentsubmenu').removeClass('hide');
    }
});