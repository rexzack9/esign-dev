var sign1 = false;
var sign2 = false;
var sign3 = false;
var startX;
var startY;
var fileName = '';
$pdfUrl = '';
$imgUrl = '';
$mydocumentList = $('#doc-list');
$imgWidth = '';
$imgHeight = '';
$docId = '';

$userList = [];

$recipientList = ['recipient1'];
$recipientCount = 1;

var __PDF_DOC,
    __CURRENT_PAGE,
    __TOTAL_PAGES,
    __PAGE_RENDERING_IN_PROGRESS = 0,
    __CANVAS = $('#pdf-canvas').get(0),
    __CANVAS_CTX = __CANVAS.getContext('2d');

getAllUser();

$("#recipient1").select2({
    width: 300,
});

$('#continue').on('click', function () {
    $('.success-file').addClass('hide');
    $('.select-sign').removeClass('hide');
});

$('.nextOthers').on('click', function () {
    $('#nav-title').addClass('hide');
    $('.select-sign').addClass('hide');
    $('.pdf-view').removeClass('hide');

    $('.others-view').addClass('hide');
    $('.others-sign').addClass('hide');
    $('#pdf-canvas').removeClass('hide');
    showPDF($pdfUrl);
});

$('.sign1').on('click', function () {
    $(this).css('background-color', '#F2F2F2').addClass('border-none');
    $('.sign2').css('background-color', 'white').removeClass('border-none');
    $('.sign3').css('background-color', 'white').removeClass('border-none');
    $('#recipient-view').addClass('hide');
    console.log('sign1');
    sign1 = true;
    sign2 = false;
    sign3 = false;

    $('#nav-title').addClass('hide');
    $('.select-sign').addClass('hide');
    $('.pdf-view').removeClass('hide');

    // var dummyPdf = 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf';
    // $pdfUrl = dummyPdf;
    // showPDF($pdfUrl);
    $('#pdf-canvas').removeClass('hide');
    showPDF($pdfUrl);
});

$('.sign2').on('click', function () {
    $(this).css('background-color', '#F2F2F2').addClass('border-none');
    $('.sign1').css('background-color', 'white').removeClass('border-none');
    $('.sign3').css('background-color', 'white').removeClass('border-none');
    $('#recipient-view').removeClass('hide');
    console.log('sign2');
    sign1 = false;
    sign2 = true;
    sign3 = false;

    $('#nav-title').addClass('hide');
    $('.others-view').removeClass('hide').find('span').html('Me & Others');
    $('.select-sign').addClass('hide');
    $('.others-sign').removeClass('hide');
    // $('.pdf-view').removeClass('hide');
});

$('.sign3').on('click', function () {
    $(this).css('background-color', '#F2F2F2').addClass('border-none');
    $('.sign1').css('background-color', 'white').removeClass('border-none');
    $('.sign2').css('background-color', 'white').removeClass('border-none');
    $('#recipient-view').removeClass('hide');
    console.log('sign3');
    sign1 = false;
    sign2 = false;
    sign3 = true;

    $('#nav-title').addClass('hide');
    $('.others-view').removeClass('hide').find('span').html('Others');
    $('.select-sign').addClass('hide');
    $('.others-sign').removeClass('hide');
    // $('.pdf-view').removeClass('hide');
});

$('.back').on('click', function () {
    $('.select-sign').removeClass('hide');
    $('#nav-title').removeClass('hide');
    $('.others-view').addClass('hide').find('span').html('');
    $('.others-sign').addClass('hide');
    $('.pdf-view').addClass('hide');
});

$('.add-recipient').on('click', function () {
    // $recipientBody = $('#recipient-body');
    // var div = '';
    // div += '<div class="d-flex mt-3">';
    // div += '<div style="width: 100%;">';
    // div += '<select id="recipient1" class="form-control"></select>';
    // div += '</div>';
    // div += '<div class="my-auto ml-3">';
    // div += '<i class="fa fa-trash delete-recipient"></i>';
    // div += '</div>';
    // div += '</div>';
    // $recipientBody.append(div);
    $recipientCount++;
    $recipientList.push('recipient' + $recipientCount);
    $recipientBody = $('#recipient-body');
    $recipientBody.append('<div class="mt-3"><select id="recipient' + $recipientCount + '" class="form-control"></select></div>');
    // $recipientBody.append("<div id='recipientdiv" + $recipientCount + "' class='mb-4 d-flex' style='border: 1px solid lightgray; width: 50%; padding: 6px'><select class='selectpicker' id='recipient" + $recipientCount + "' style='width: 100%;'></select><div class='my-auto ml-3 pointer removeRecipient'><i class='fa fa-minus-circle red-text'></i></div></div>");
    $('#recipient' + $recipientCount).select2({
        width: 300,
    });
    drawUsers('recipient' + $recipientCount);
});

$('#recipient-body').delegate('.delete-recipient', 'click', function () {
    $(this).parent().parent().remove();
});

$('#addRecipient').on('click', function () {
    $recipientCount++;
    $recipientList.push('recipient' + $recipientCount);
    $recipientBody = $('#recipientBody');
    $recipientBody.append("<div id='recipientdiv" + $recipientCount + "' class='mb-4 d-flex' style='border: 1px solid lightgray; width: 50%; padding: 6px'><select class='selectpicker' id='recipient" + $recipientCount + "' style='width: 100%;'></select><div class='my-auto ml-3 pointer removeRecipient'><i class='fa fa-minus-circle red-text'></i></div></div>");
    $('#recipient' + $recipientCount).select2();
    drawUsers('recipient' + $recipientCount);
});

$('#recipientBody').delegate('.removeRecipient', 'click', function () {
    var divId = $(this).parent().attr('id');
    var recipientId = $('#' + divId + ' .selectpicker').attr('id');
    $('#' + divId).remove();
    $recipientList = jQuery.grep($recipientList, function (value) {
        return value != recipientId;
    });
});

$('#saveDoc').on('click', function () {
    if ($('#chooseFile')[0].files.length != 0) {
        if (sign1 || sign2) {
            $('#save-doc-view').css('display', 'none');
            $('#prepare-doc-view').removeClass('hide');
            var documentFile = $('#chooseFile').prop('files');
            if (sign1) {
                console.log('just me');
                var formData = new FormData();
                formData.append('title', fileName);
                formData.append('documentfile', documentFile[0]);
                formData.append('userid', $userId);
                // formData.append('cc', '');
                addDocument(formData).then(function (response) {
                    console.log(response);
                });
            }
            if (sign2) {
                console.log('me and other');
                var temp = [];
                var temp2 = [];
                $.each($recipientList, function (i, data) {
                    var value = $('#' + data).val();
                    temp2.push({
                        'id': value,
                        'status': '',
                        'time': '',
                    });
                });
                temp = {
                    'cc': temp2,
                };
                var ccString = JSON.stringify(temp);
                var formData = new FormData();
                formData.append('title', fileName);
                formData.append('documentfile', documentFile[0]);
                formData.append('userid', $userId);
                formData.append('cc', ccString);
                addDocument(formData).then(function (response) {
                    console.log(response);
                });
            }
        }
    }
    $('#save-doc-view').css('display', 'none');
    $('#prepare-doc-view').removeClass('hide');
});

$('#upload').on('click', function () {
    console.log('click')
    $('#chooseFile').trigger('click');
});

$('#chooseFile').change(function (e) {
    $('.select-file').addClass('hide');
    $('.success-file').removeClass('hide');
    fileName = e.target.files[0].name;
    $('#fileName').html(fileName);
});

$('#prepareDoc').on('click', function () {
    getPrepareDocs().then(function (response) {
        if (response['status'] == 'success') {
            var data = response['value'];
            $mydocumentList.html('');
            $.each(data, function (i, data) {
                var row = '';
                row += '<div id=list' + i + ' class="document-box mb-2 d-flex pointer">';
                row += '<div class="my-auto">';
                row += '<i class="fa fa-file mr-2"></i>';
                row += '</div>';
                row += '<div class="my-auto">';
                row += data['title'];
                row += '</div>';
                row += '</div>';
                $mydocumentList.append(row);
                $('#list' + i).data('data', data);
            });
        }
    });
    $('#prepare-doc-view').addClass('hide');
    $('#doc-list-view').removeClass('hide');
});

$('#cancel-preview').on('click', function () {
    // $('#doc-list-view').removeClass('hide');
    // $('#preview-doc').addClass('hide');
    $('#pdfModal').modal('hide');
});

// $('#sendDocument').on('click', function () {
//     $('#sendModal').modal({
//         backdrop: 'static',
//         keyboard: false,
//     });
// });

$('#sendOkay').on('click', function () {
    $('#sendModal').modal('hide');
    window.location.reload();
});

$mydocumentList.delegate('.document-box', 'click', function () {
    var data = $(this).data('data');
    $docId = data['id'];
    // $('#doc-list-view').addClass('hide');
    // $('#preview-doc').removeClass('hide');
    var dummyPdf = 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf';
    $pdfUrl = data['file'];
    showPDF($pdfUrl);
    // console.log('show pdf');
    $('#pdfModal').modal({ backdrop: 'static', keyboard: false });
});

function getAllUser() {
    allUsers().then(function (response) {
        if (response.length != 0) {
            $userList = response;
            drawUsers('recipient1');
        }
    });
}

function drawUsers(recipientId) {
    $recipientBody = $('#' + recipientId);
    $recipientBody.html();
    $.each($userList, function (i, data) {
        $recipientBody.append('<option value=' + data['id'] + '>' + data['name'] + '</option>');
    });
}
