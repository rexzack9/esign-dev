<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/fontawesome/css/all.css">
<link rel="stylesheet" href="assets/custom/css/style.css">
<link rel="stylesheet" href="assets/select2-4.0.12/dist/css/select2.min.css">
<style>
    .select2-container--default .select2-selection--single {
        background-color: transparent !important;
        border: none !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        border-bottom: 1px solid black !important;
    }
</style>
<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<body class="bg-eSign">
    <div class="home-view">
        <!-- header -->
        <div class="brb-20 custom-grey" style="margin: 0; padding-top: 10px; padding-bottom: 80px;">
            <div class="col-md-6 offset-md-3 d-flex justify-content-between" style="margin-top: 50px;">
                <div class="my-auto" style="font-size: 18px;">
                    eSignature
                </div>
                <div class="my-auto">
                    <button onclick="logout()" class="btn f-10">Logout</button>
                </div>
            </div>
            <div class="d-flex justify-content-center" style="margin-top: 20px;">
                <div onclick="changeView('process')" class="custom-yellow custom-card-menu p-3 my-auto">
                    <div>
                        <img src="assets/icon/clock.png">
                    </div>
                    <div class="f-10" style="margin-top: 5px; color: white;">
                        <span>In process</span>
                    </div>
                    <div>
                        <span class="menu-count" id="processCount">0</span>
                    </div>
                </div>
                <div onclick="changeView('rejected')" class="custom-red custom-card-menu p-3 my-auto">
                    <div>
                        <img src="assets/icon/x.png">
                    </div>
                    <div class="f-10" style="margin-top: 5px; color: white;">
                        <span>Rejected</span>
                    </div>
                    <div>
                        <span class="menu-count" id="rejectedCount">0</span>
                    </div>
                </div>
                <div onclick="changeView('completed')" class="custom-green custom-card-menu p-3 my-auto">
                    <div>
                        <img src="assets/icon/check-white.png">
                    </div>
                    <div class="f-10" style="margin-top: 5px; color: white;">
                        <span>Completed</span>
                    </div>
                    <div>
                        <span class="menu-count" id="completedCount">0</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- body -->
        <div class="d-flex justify-content-center">
            <div onclick="chooseFile()" class="btn-add-document custom-blue d-flex">
                <div class="d-flex my-auto ml-1" style="border: 1px solid white; border-style: dashed; height: 52px; width: 52px; border-radius: 10px;">
                    <div class="my-auto mx-auto">
                        <img src="assets/icon/plus.png">
                    </div>
                </div>
                <div class="my-auto ml-3" style="color: white;">
                    Add Document
                </div>
            </div>
            <input id="documentFile" type="file" hidden>
        </div>

        <div class="process-view col-md-6 offset-md-3" style="margin-top: 40px; padding-bottom: 40px;">
            <span style="font-size: 12px;">In Process</span>
            <center>
                <div id="processEmpty">
                    <img src="assets/images/emptyState.png">
                </div>
                <div id="processBody">
                    <!-- <div class="white-box mt-3 br-10 pointer" style="max-width: 312px;">
                    <div class="d-flex justify-content-between p-3">
                    <div class="d-flex">
                    <div class="my-auto">
                    <img src="assets/icon/file-text.png">
                    </div>
                    <div class="my-auto ml-3">
                    <div class="document-name" style="text-align: left;">
                    <span id="">1234567.pdf</span>
                    </div>
                    <div class="document-date">
                    10/12/2020
                    </div>
                    </div>
                    </div>
                    <div>
                    <img src="assets/feather/chevron-right.svg">
                    </div>
                    </div>
                    <div style="height: 1px; background-color: lightgray; width: 100%;"></div>
                    <div class="p-3">
                    <div class="history-btn" style="font-size: 10px;">
                    VIEW HISTORY
                    </div>
                    <div class="history-view hide">
                    <div style="font-size: 10px; color: #9b51e0;">
                    COLLAPSE
                    </div>
                    <div class="mt-3" style="font-size: 10px; color: #828282; text-align: left;">
                    Created by epul
                    </div>
                    </div>
                    </div>
                    </div> -->
                </div>
            </center>
        </div>

        <div class="rejected-view hide col-md-6 offset-md-3" style="margin-top: 40px; padding-bottom: 40px;">
            <span style="font-size: 12px;">Rejected</span>
            <center>
                <div id="rejectedEmpty">
                    <img src="assets/images/emptyState.png">
                </div>
                <div id="rejectedBody">
                </div>
            </center>
        </div>

        <div class="completed-view hide col-md-6 offset-md-3" style="margin-top: 40px; padding-bottom: 40px;">
            <span style="font-size: 12px;">Completed</span>
            <center>
                <div id="completedEmpty">
                    <img src="assets/images/emptyState.png">
                </div>
                <div id="completedBody">
                </div>
            </center>
        </div>
    </div>

    <!-- add document view -->
    <div class="addDocument-view hide">
        <div class="d-flex flex-column" style="margin: 0;">
            <div class="col-md-6 offset-md-3 mt-5">
                <div style="font-size: 18px;">
                    Added Document
                </div>
                <div class="d-flex white-box mt-5 p-3 br-10">
                    <div class="my-auto">
                        <img src="assets/icon/file-text.png">
                    </div>
                    <div class="my-auto ml-3">
                        <div class="document-name">
                            <span id="fileName"></span>
                        </div>
                        <div class="document-date">
                            28 July 2020 . 2:42 PM
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <button onclick="changeView('recipient')" class="btn btn-block custom-btn-blue br-100">CONTINUE</button>
                </div>
            </div>
        </div>
    </div>

    <!-- add recipient view -->
    <div class="addRecipient-view hide">
        <div class="d-flex flex-column" style="margin: 0;">
            <div class="col-md-6 offset-md-3 mt-5">
                <div class="d-flex justify-content-between">
                    <div style="font-size: 18px;">
                        Added Recipient
                    </div>
                    <div>
                        <button onclick="changeView('pdf')" id="checkRecipient" class="btn hide">
                            <img src="assets/feather/check.svg">
                        </button>
                    </div>
                </div>
                <div id="recipientBody" class="mt-4">
                </div>
                <div class="d-flex justify-content-center mt-4">
                    <div onclick="addRecipient()" class="btn-add-recipient custom-peach d-flex">
                        <div class="d-flex my-auto ml-1" style="border: 1px solid black; border-style: dashed; height: 52px; width: 52px; border-radius: 10px;">
                            <div class="my-auto mx-auto">
                                <img src="assets/feather/plus.svg">
                            </div>
                        </div>
                        <div class="my-auto ml-3">
                            Recipient Email
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--annotation Modal -->
    <div class="modal fade" id="annotationModal" tabindex="-1" role="dialog" aria-labelledby="annotationModalTitle" aria-hidden="true" style="width: max-content; min-width: 400px;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content mx-auto">
                <div class="modal-header" style="background-color: #f2f2f2">
                    <h5 class="modal-title" id="exampleModalLongTitle">Annotation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mt-4 row">
                        <select id="annotation" class="form-control">
                            <!-- <option value="1">Hafizal</option>
                            <option value="3">Saiful</option>
                            <option value="4">Lee</option>
                            <option value="5">Sarah</option>
                            <option value="6">Fateha</option> -->
                        </select>
                    </div>
                    <div class="mt-5 d-flex justify-content-end">
                        <div id="okayBtn" onclick="confirmAnnotation()" class="custom-btn orange-btn br-6 d-flex fs-14">
                            <div class="my-auto mx-auto">
                                OKAY
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--done Modal -->
    <div class="modal fade" id="doneModal" tabindex="-1" role="dialog" aria-labelledby="doneModalTitle" aria-hidden="true" style="width: max-content; min-width: 400px;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content mx-auto">
                <div class="modal-body">
                    <div class="mt-4">
                        <div class="mx-auto" style="text-align: center;">
                            <img src="assets/feather/check-circle.svg">
                        </div>
                        <div class="mt-3 mx-auto" style="color: #828282; text-align: center;">
                            The document is sent
                        </div>
                    </div>
                    <div class="mt-5" style="text-align: center;">
                        <button id="done" class="btn br-6 fs-14" style="background-color: #8e2fed; color: white;">
                            DONE
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--tac Modal -->
    <div class="modal fade" id="tacModal" tabindex="-1" role="dialog" aria-labelledby="tacModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content mx-auto">
                <div class="modal-header" style="background-color: #f2f2f2">
                    <h5 class="modal-title" id="exampleModalLongTitle">Verification</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mt-4 row">
                        <div class="col-4 offset-1">
                            <img src="assets/images/smartphone.svg" style="width: 60px;">
                        </div>
                        <div class="col-6">
                            <div>
                                To proceed, please request and enter your TAC Number.
                            </div>
                            <div class="mt-3">
                                <a href="#" class="link-text"><u>Request Tac</u></a>
                            </div>
                            <div>
                                <div class="form-group">
                                    <input id="tac" type="text" class="border-bottom-only w-max" placeholder="Enter TAC">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-5 d-flex justify-content-end">
                        <div id="confirmTac" class="custom-btn orange-btn br-6 d-flex fs-14">
                            <div class="my-auto mx-auto">
                                Confirm
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- pdf view -->
    <div class="reupload-view hide">
        <div class="d-flex flex-column custom-grey" style="margin: 0;">
            <div class="col-md-6 offset-md-3 mt-5 d-flex justify-content-between">
                <div id="backBtn" class="my-auto" style="font-size: 18px;">
                    <img src="assets/feather/arrow-left.svg">
                </div>
                <div onclick="changeView('rejected')" class="my-auto" style="padding: 8px 16px 8px 16px; border-top-left-radius: 20px; border-bottom-left-radius: 20px; background-color: peachpuff; margin-right: -15px;">
                    <img src="assets/feather/home.svg">
                </div>
            </div>
        </div>
    </div>
    <div class="pdf-view hide">
        <canvas id="pdf-canvas" style="max-width: 100%" class="mx-auto hide"></canvas>

        <div id="canvasPlaceholder">

        </div>

        <div id="pageBtn" class="d-flex justify-content-between p-3">
            <div id="previousBtn" onclick="changePage('previous')" class="pointer">
                Previous Page
            </div>
            <div>
                <span id="currentPage"></span> / <span id="totalPages"></span>
            </div>
            <div id="nextBtn" onclick="changePage('next')" class="pointer">
                Next Page
            </div>
        </div>

        <div class="mt-2">
            <div class="finish-btn hide" style="padding: 16px;">
                <button id="finishBtn" onclick="sendDocument()" class="btn btn-block custom-btn-blue br-100">Finish</button>
            </div>

            <div class="reupload-btn hide" style="padding: 16px;">
                <button onclick="reuploadDocument()" class="btn btn-block custom-btn-blue br-100">RE-UPLOAD</button>
                <input id="reuploadFile" type="file" hidden>
            </div>

            <div class="resend-btn hide" style="padding: 16px;">
                <button onclick="resendDocument()" class="btn btn-block custom-btn-blue br-100">RESEND</button>
            </div>
        </div>
        <!-- <div class="d-flex flex-column" style="margin: 0;">
            <div class="col-md-6 offset-md-3 mt-5">
                <div class="d-flex justify-content-between">
                    <div style="font-size: 18px;">
                        pdf view
                    </div>
                </div>
            </div>
        </div> -->
    </div>

    <!--success submit Modal -->
    <div class="modal fade" id="successSubmitModal" tabindex="-1" role="dialog" aria-labelledby="successSubmitModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content mx-auto" style="width: max-content; padding: 16px;">
                <div class="modal-body">
                    <div class="mt-4">
                        <div class="d-flex">
                            <img class="mx-auto" src="assets/icon/check-purple.png">
                        </div>
                        <div class="mt-4">
                            <div style="font-size: 12px;">
                                The document is submitted.
                            </div>
                        </div>
                    </div>
                    <div class="mt-5 d-flex justify-content-center">
                        <div onclick="window.location.reload()" class="custom-btn orange-btn br-6 d-flex fs-14" style="background: #8E2FED; color: white; padding: 8px; border-radius: 6px;">
                            <div class="my-auto mx-auto">
                                DONE
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>

<script src="assets/jquery/jquery-3.5.0.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/custom/js/connection.js"></script>
<script src="assets/custom/js/custom.js"></script>

<script src="assets/pdfjs/pdf.js"></script>
<script src="assets/pdfjs/pdf.worker.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://unpkg.com/pdf-lib@1.4.0"></script>
<script src="https://unpkg.com/downloadjs@1.4.7"></script>
<script src="assets/signature/js/jquery.signature.js"></script>
<script src="assets/select2-4.0.12/dist/js/select2.min.js"></script>
<script src="assets/dom-to-image/dom-to-image.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script> -->

<script src="assets/custom/js/sign-document.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>

<script>
    // $baseUrl = 'http://52.74.178.166:82';

    $baseUrl = 'https://codeviable.com/engine-signature/public';

    // $pdfUrl = 'https://pdf-lib.js.org/assets/with_update_sections.pdf';
    $pdfUrl = '';
    // $imgUrl = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Instagram_icon.png/1200px-Instagram_icon.png';
    $imgUrl = '';
    $textUrl = '';
    $commentText = '';
    $textWidth = 0;
    $textHeight = 0;
    $imgWidth = 0;
    $imgHeight = 0;
    $textPosX = 0;
    $textPosY = 0;
    $imgPosX = 0;
    $imgPosY = 0;

    $recipientCount = 0;
    $recipientDomList = [];
    $recipientIdList = [];

    $isImage = false;
    $isText = false;

    $lastX = 0;
    $lastY = 0;

    $user = null;
    $token = null;

    $tempCc = [];
    $postData = [];

    $processList = [];
    $rejectedList = [];
    $completedList = [];

    $userList = [];

    $currentPdfData = '';

    $isDraggable = true;

    $totalPages = 0;
    $currentPage = 1;

    function confirmAnnotation() {
        var id = $('#annotation').val();
        var index = $recipientIdList.indexOf(id);
        if (index > -1) {
            $recipientIdList.splice(index, 1);
        }

        $('#annotation').html('');
        $.each($recipientIdList, function(i, data) {
            var id = data;
            console.log(data);
            $.each($userList, function(i2, data2) {
                if (data2['id'] == id) {
                    var row = '';
                    row += '<option value=' + data2['id'] + '>' + data2['email'] + '</option>';
                    $('#annotation').append(row);
                }
            });
        });

        $tempCc.push({
            'id': id,
            'status': 'process',
            'coordinatex': $lastX,
            'coordinatey': $lastY,
            'turn': '',
        });

        $postData = {
            'cc': $tempCc,
        };

        console.log($postData);

        $('#annotationModal').modal('hide');

    }

    function sendDocument() {
        $('#finishBtn').attr('disabled', true);

        cc = JSON.stringify($postData);

        var formData = new FormData();
        formData.append('userid', $user['id']);
        formData.append('documentfile', $('#documentFile').prop('files')[0]);
        formData.append('cc', cc);

        $.ajax({
            url: $baseUrl + '/document/add',
            method: 'POST',
            data: formData,
            headers: {
                'Authorization': 'Bearer ' + $token,
            },
            processData: false,
            contentType: false,
        }).done(function(response) {
            // $('#finishBtn').attr('disabled', true);
            $('#doneModal').modal({
                backdrop: 'static',
                keyboard: false,
            });
            console.log(response);
        });
    }

    function logout() {
        window.location.href = './';
    }

    function chooseFile() {
        $('#documentFile').trigger('click');
    }

    function changeView(view) {
        $('.finish-btn').hide();
        $('.reupload-btn').hide();
        $('.resend-btn').hide();
        $('.reupload-view').hide();
        $('.pdf-view').hide();

        if (view == 'process') {
            $('.home-view').show();
            $('.process-view').show();
            $('.rejected-view').hide();
            $('.completed-view').hide();
        } else if (view == 'rejected') {
            $('.home-view').show();
            $('.process-view').hide();
            $('.rejected-view').show();
            $('.completed-view').hide();
        } else if (view == 'completed') {
            $('.home-view').show();
            $('.process-view').hide();
            $('.rejected-view').hide();
            $('.completed-view').show();
        } else if (view == 'recipient') {
            $('body').removeClass('bg-eSign').addClass('custom-grey');
            $('.addDocument-view').hide();
            $('.addRecipient-view').show();
        } else if (view == 'pdf') {

            $.each($recipientDomList, function(i, data) {
                var value = $('#' + data).val();
                $recipientIdList.push(value);
            });

            $('#annotation').html('');
            $.each($recipientIdList, function(i, data) {
                var id = data;
                $.each($userList, function(i2, data2) {
                    if (data2['id'] == id) {
                        var row = '';
                        row += '<option value=' + data2['id'] + '>' + data2['email'] + '</option>';
                        $('#annotation').append(row);
                    }
                });
            });

            $('.addRecipient-view').hide();
            $('.pdf-view').show();
            $('.finish-btn').show();
            showPDF($pdfUrl, 'submit');
            var newCanvas = document.getElementById('pdf-canvas');
            var newHeight = newCanvas.height;
            console.log($(newHeight));
        }
    }

    function addRecipient() {
        $recipientCount++;
        $('#checkRecipient').removeClass('hide');
        var row = '';
        // row += '<input id="testing" type="email" class="custom-text-input mb-3" placeholder="Email">';
        row += '<div class="d-flex justify-content-center mb-3"><select id="recipient' + $recipientCount + '" class="custom-text-input"></select></div>'
        $('#recipientBody').append(row);
        $recipientDomList.push('recipient' + $recipientCount);

        $("#recipient" + $recipientCount).select2({
            width: 300,
        });

        if ($userList['status'] != null) {
            console.log('ini kali lahhh');
            $.each($userList['value'], function(i, data) {
                var row2 = '';
                if (data['role'] == 'signatory') {
                    row2 += '<option value=' + data['id'] + '>' + data['email'] + '</option>';
                }
                $('#recipient' + $recipientCount).append(row2);
            });
        } else {
            $.each($userList, function(i, data) {
                var row2 = '';
                row2 += '<option value=' + data['id'] + '>' + data['email'] + '</option>';
                $('#recipient' + $recipientCount).append(row2);
            });
        }
    }

    function getListDocuments(status) {
        $.ajax({
            url: $baseUrl + '/document/listdocument?userid=' + $user['id'] + '&status=' + status,
            headers: {
                'Authorization': 'Bearer ' + $token,
            },
        }).done(function(response) {
            if (response['status'] == 'success') {
                var data = response['value'];
                $('#processCount').html(data['count']['totalprocess']);
                $('#rejectedCount').html(data['count']['totalrejected']);
                $('#completedCount').html(data['count']['totalcompleted']);

                if (status == 'process') {
                    if (data['listbystatus'].length != 0) {
                        $('#processEmpty').hide();
                        $processList = data['listbystatus'];
                        $.each($processList, function(i, list) {
                            getHistory(list['id']);

                            var fileName = list['file'].substr(list['file'].lastIndexOf('/') + 1);
                            var row = '';
                            // row += '<div class="d-flex justify-content-between white-box mt-3 p-3 br-10 pointer" style="max-width: 312px;">';
                            // row += '<div class="d-flex">';
                            // row += '<div class="my-auto">';
                            // row += '<img src="assets/icon/file-text.png">';
                            // row += '</div>';
                            // row += '<div class="my-auto ml-3">';
                            // row += '<div class="document-name" style="text-align: left;">';
                            // row += '<span id="">' + fileName + '</span>';
                            // row += '</div>';
                            // row += '<div class="document-date">';
                            // row += list['date'] + ' . ' + list['time'];
                            // row += '</div>';
                            // row += '</div>';
                            // row += '</div>';
                            // row += '<div>';
                            // row += '<img src="assets/feather/chevron-right.svg">';
                            // row += '</div>';
                            // row += '</div>';

                            row += '<div class="white-box mt-3 br-10 pointer" style="max-width: 312px;">';
                            row += '<div class="d-flex justify-content-between p-3">';
                            row += '<div class="d-flex">';
                            row += '<div class="my-auto">';
                            row += '<img src="assets/icon/file-text.png">';
                            row += '</div>';
                            row += '<div class="my-auto ml-3">';
                            row += '<div class="document-name" style="text-align: left;">';
                            row += '<span id="">' + fileName + '</span>';
                            row += '</div>';
                            row += '<div class="document-date">';
                            row += list['date'] + ' . ' + list['time'];
                            row += '</div>';
                            row += '</div>';
                            row += '</div>';
                            row += '<div>';
                            row += '<img src="assets/feather/chevron-right.svg">';
                            row += '</div>';
                            row += '</div>';
                            row += '<div style="height: 1px; background-color: lightgray; width: 100%;"></div>';
                            row += '<div class="p-3">';
                            row += '<div data-id=' + list['id'] + ' class="history-btn doc' + list['id'] + '" style="font-size: 10px;">';
                            row += 'VIEW HISTORY';
                            row += '</div>';
                            row += '<div data-id=' + list['id'] + ' class="history-view doc-view' + list['id'] + ' hide">';
                            row += '<div style="font-size: 10px; color: #9b51e0;">';
                            row += 'COLLAPSE';
                            row += '</div>';
                            row += '<div id="historyBody' + list['id'] + '" class="mt-3" style="font-size: 10px; color: #828282; text-align: left;">';
                            row += '</div>';
                            row += '</div>';
                            row += '</div>';
                            row += '</div>';

                            $('#processBody').append(row);
                        });
                    } else {
                        $('#processEmpty').show();
                    }
                } else if (status == 'rejected') {
                    $rejectedList = data['listbystatus'];
                    if (data['listbystatus'].length != 0) {
                        $('#rejectedEmpty').hide();
                        $rejectedList = data['listbystatus'];
                        $.each($rejectedList, function(i, list) {
                            getHistory(list['id']);
                            var fileName = list['file'].substr(list['file'].lastIndexOf('/') + 1);
                            var row = '';
                            // row += '<div id="rejectedDocument' + list['id'] + '" class="d-flex justify-content-between white-box mt-3 p-3 br-10 pointer view" style="max-width: 312px;">';
                            // row += '<div class="d-flex">';
                            // row += '<div class="my-auto">';
                            // row += '<img src="assets/icon/file-text.png">';
                            // row += '</div>';
                            // row += '<div class="my-auto ml-3">';
                            // row += '<div class="document-name" style="text-align: left;">';
                            // row += '<span id="">' + fileName + '</span>';
                            // row += '</div>';
                            // row += '<div class="document-date">';
                            // row += list['date'] + ' . ' + list['time'];
                            // row += '</div>';
                            // row += '</div>';
                            // row += '</div>';
                            // row += '<div>';
                            // row += '<img src="assets/feather/chevron-right.svg">';
                            // row += '</div>';
                            // row += '</div>';
                            row += '<div id="rejectedDocument' + list['id'] + '" class="white-box mt-3 br-10 pointer view" style="max-width: 312px;">';
                            row += '<div class="d-flex justify-content-between p-3">';
                            row += '<div class="d-flex">';
                            row += '<div class="my-auto">';
                            row += '<img src="assets/icon/file-text.png">';
                            row += '</div>';
                            row += '<div class="my-auto ml-3">';
                            row += '<div class="document-name" style="text-align: left;">';
                            row += '<span id="">' + fileName + '</span>';
                            row += '</div>';
                            row += '<div class="document-date">';
                            row += list['date'] + ' . ' + list['time'];
                            row += '</div>';
                            row += '</div>';
                            row += '</div>';
                            row += '<div>';
                            row += '<img src="assets/feather/chevron-right.svg">';
                            row += '</div>';
                            row += '</div>';
                            row += '<div style="height: 1px; background-color: lightgray; width: 100%;"></div>';
                            row += '<div class="p-3">';
                            row += '<div data-id=' + list['id'] + ' class="history-btn doc' + list['id'] + '" style="font-size: 10px;">';
                            row += 'VIEW HISTORY';
                            row += '</div>';
                            row += '<div data-id=' + list['id'] + ' class="history-view doc-view' + list['id'] + ' hide">';
                            row += '<div style="font-size: 10px; color: #9b51e0;">';
                            row += 'COLLAPSE';
                            row += '</div>';
                            row += '<div id="historyBody' + list['id'] + '" class="mt-3" style="font-size: 10px; color: #828282; text-align: left;">';
                            row += '</div>';
                            row += '</div>';
                            row += '</div>';
                            row += '</div>';
                            $('#rejectedBody').append(row);
                            $('#rejectedDocument' + list['id']).data('data', list);
                        });
                    } else {
                        $('#rejectedEmpty').show();
                    }
                } else if (status == 'completed') {
                    $completedList = data['listbystatus'];
                    if (data['listbystatus'].length != 0) {
                        $('#completedEmpty').hide();
                        $completedList = data['listbystatus'];
                        $.each($completedList, function(i, list) {
                            getHistory(list['id']);
                            var fileName = list['file'].substr(list['file'].lastIndexOf('/') + 1);
                            var row = '';
                            // row += '<div class="d-flex justify-content-between white-box mt-3 p-3 br-10 pointer" style="max-width: 312px;">';
                            // row += '<div class="d-flex">';
                            // row += '<div class="my-auto">';
                            // row += '<img src="assets/icon/file-text.png">';
                            // row += '</div>';
                            // row += '<div class="my-auto ml-3">';
                            // row += '<div class="document-name" style="text-align: left;">';
                            // row += '<span id="">' + fileName + '</span>';
                            // row += '</div>';
                            // row += '<div class="document-date">';
                            // row += list['date'] + ' . ' + list['time'];
                            // row += '</div>';
                            // row += '</div>';
                            // row += '</div>';
                            // row += '<div>';
                            // row += '<img src="assets/feather/chevron-right.svg">';
                            // row += '</div>';
                            // row += '</div>';
                            row += '<div class="white-box mt-3 br-10 pointer" style="max-width: 312px;">';
                            row += '<div class="d-flex justify-content-between p-3">';
                            row += '<div class="d-flex">';
                            row += '<div class="my-auto">';
                            row += '<img src="assets/icon/file-text.png">';
                            row += '</div>';
                            row += '<div class="my-auto ml-3">';
                            row += '<div class="document-name" style="text-align: left;">';
                            row += '<span id="">' + fileName + '</span>';
                            row += '</div>';
                            row += '<div class="document-date">';
                            row += list['date'] + ' . ' + list['time'];
                            row += '</div>';
                            row += '</div>';
                            row += '</div>';
                            row += '<div>';
                            row += '<img src="assets/feather/chevron-right.svg">';
                            row += '</div>';
                            row += '</div>';
                            row += '<div style="height: 1px; background-color: lightgray; width: 100%;"></div>';
                            row += '<div class="p-3">';
                            row += '<div data-id=' + list['id'] + ' class="history-btn doc' + list['id'] + '" style="font-size: 10px;">';
                            row += 'VIEW HISTORY';
                            row += '</div>';
                            row += '<div data-id=' + list['id'] + ' class="history-view doc-view' + list['id'] + ' hide">';
                            row += '<div style="font-size: 10px; color: #9b51e0;">';
                            row += 'COLLAPSE';
                            row += '</div>';
                            row += '<div id="historyBody' + list['id'] + '" class="mt-3" style="font-size: 10px; color: #828282; text-align: left;">';
                            row += '</div>';
                            row += '</div>';
                            row += '</div>';
                            row += '</div>';
                            $('#completedBody').append(row);
                        });
                    } else {
                        $('#completedEmpty').show();
                    }
                }
            }
        })
    }

    function getHistory(id) {
        $('#historyBody' + id).html('');
        documentHistory(id).then(function(response) {
            if (response['status'] == 'success') {
                var history = response['value'];
                $.each(history, function(i, data) {
                    var row = '';
                    row += '<div class="mb-2 d-flex"><div class="my-auto mr-2" style="height: 6px; width: 6px; border-radius: 50%; background-color: #FFCAA4;"></div class="my-auto"> ' + data['message'] + '</div>';
                    $('#historyBody' + id).append(row);
                });
            }
        });
    }

    $(document).ready(function() {
        $sig = $('#sign-canvas').signature({
            background: 'transparent'
        });

        $user = JSON.parse(localStorage.getItem('eSignUser'));
        $token = JSON.parse(localStorage.getItem('eSignToken'));

        getAllUser();

        getListDocuments('process');
        getListDocuments('rejected');
        getListDocuments('completed');


        $('#documentFile').change(function(e) {
            // window.location.href = 'setup-document.html';
            fileName = e.target.files[0].name;
            $('#fileName').html(fileName);

            var pdfFile = $('#documentFile').prop('files')[0];
            $pdfUrl = URL.createObjectURL(pdfFile);

            $('#testImg').attr('src', pdfFile);

            $('body').removeClass('bg-eSign').addClass('custom-grey');
            $('.home-view').hide();
            $('.addDocument-view').show();
        });

        requestSignature();

        $('#done').on('click', function() {
            window.location.reload();
        })

        $('#applySignature').on('click', function() {
            var signImg = $sig.signature('toDataURL', 'image/png');
            $imgUrl = signImg;
            $('#sign-box').addClass('hide');
            $('#sampleImg').attr('src', signImg);
            $('#sampleImg').removeClass('hide');
        });

        $('#cancelSignature').on('click', function() {
            $('#sign-canvas').signature('destroy');
            // $sig = $('#sign-canvas').signature({
            //     background: 'transparent'
            // });
            // $('#sampleImg').attr('src', '');
            $('#sign-box').addClass('hide');
        });

        $('#addSignature').on('click', function() {
            // $('#tacModal').modal();

            $('#signModal').modal();

            // $('#sign-box').removeClass('hide');
            // $sig = $('#sign-canvas').signature({
            //     background: 'transparent'
            // });
        });

        $('#addText').on('click', function() {
            $('#textModal').modal();
        });

        $('#sign-list').delegate('.pointer', 'click', function() {
            var selectedImg = $(this).data('img');
            $imgUrl = selectedImg;
            $('#imgPlaceholder').html('<img id="sampleImg" style="width: 100px; height: auto; cursor: move" src="' + $imgUrl + '">');
            $('#sampleImg').draggable();

            // $('#sampleImg').attr('src', $imgUrl);
            // $('#sampleImg').removeClass('hide');
            $('#signModal').modal('hide');
        });

        $('#continue').on('click', function() {
            $(this).addClass('disabled-btn');
            $(this).prop('disabled', true);
            // $('#pdf-canvas').removeClass('hide');
            $('#sampleImg').draggable();
            var pdfFile = $('#chooseFile').prop('files')[0];
            var test = URL.createObjectURL(pdfFile);
            $pdfUrl = test;
            console.log(test);
            // showPDF(test);
            // modifyPdf();
        });

        $('#sendDocument').on('click', function() {
            // $imgUrl = $('#temp').children('img').attr('src');
            // modifyPdf();
            // $('#sendModal').modal({
            //     backdrop: 'static',
            //     keyboard: false,
            // });

            $('#doneModal').modal();
        });

        $('#textOkay').on('click', function() {
            $('#sampleText').html('');
            var text = $('#inputText').val();
            $commentText = text;
            $('#sampleText').html(text);
            var dom = document.getElementById('sampleText');
            domtoimage.toPng(dom).then(function(img) {
                // var base64data = img.slice(img.lastIndexOf(',') + 1);
                $textUrl = img;
                $('#sampleText').html('<img id="imgText" src=' + img + '>');
            });
            $('#sampleText').draggable();

            $('#textModal').modal('hide');
        });

        $('#backBtn').on('click', function() {
            if ($('.reupload-btn').hasClass('hide')) {
                $('.reupload-btn').removeClass('hide').show();
                $('.resend-btn').addClass('hide').hide();
                showPDF($currentPdfData['file'], 'reupload');
            } else {
                changeView('rejected');
            }
        });

        $('#rejectedBody').delegate('.view', 'click', function() {
            var data = $(this).data('data');
            console.log(data);
            $currentPdfData = data;
            $('.home-view').hide();
            $('.pdf-view').show();
            $('.reupload-btn').show().removeClass('hide');
            $('.reupload-view').show().removeClass('hide');
            showPDF(data['file'], 'reupload');
            var newCanvas = document.getElementById('pdf-canvas');
            var newHeight = newCanvas.height;
            console.log($(newHeight));
        });

        $('#reuploadFile').change(function(e) {
            // window.location.href = 'setup-document.html';
            $('.reupload-btn').addClass('hide').hide();
            $('.resend-btn').removeClass('hide').show();
            console.log(e.target.files.length);
            fileName = e.target.files[0].name;
            console.log(fileName);
            // $('#fileName').html(fileName);

            var pdfFile = $('#reuploadFile').prop('files')[0];
            $pdfUrl = URL.createObjectURL(pdfFile);

            showPDF($pdfUrl, 'reupload');

            // $('#testImg').attr('src', pdfFile);

            // $('body').removeClass('bg-eSign').addClass('custom-grey');
            // $('.home-view').hide();
            // $('.addDocument-view').show();
        });

        $('#processBody').delegate('.history-btn', 'click', function() {
            var id = $(this).data('id');
            console.log(id);

            $('.doc' + id).toggleClass('hide');
            $('.doc-view' + id).toggleClass('hide');
        });

        $('#processBody').delegate('.history-view', 'click', function() {
            var id = $(this).data('id');
            console.log(id);

            $('.doc' + id).toggleClass('hide');
            $('.doc-view' + id).toggleClass('hide');
        });

        $('#rejectedBody').delegate('.history-btn', 'click', function() {
            var id = $(this).data('id');
            console.log(id);

            $('.doc' + id).toggleClass('hide');
            $('.doc-view' + id).toggleClass('hide');
        });

        $('#rejectedBody').delegate('.history-view', 'click', function() {
            var id = $(this).data('id');
            console.log(id);

            $('.doc' + id).toggleClass('hide');
            $('.doc-view' + id).toggleClass('hide');
        });

        $('#completedBody').delegate('.history-btn', 'click', function() {
            var id = $(this).data('id');
            console.log(id);

            $('.doc' + id).toggleClass('hide');
            $('.doc-view' + id).toggleClass('hide');
        });

        $('#completedBody').delegate('.history-view', 'click', function() {
            var id = $(this).data('id');
            console.log(id);

            $('.doc' + id).toggleClass('hide');
            $('.doc-view' + id).toggleClass('hide');
        });
    });

    function getAllUser() {
        allUsers().then(function(response) {
            if (response['status'] == 'success') {
                $userList = [];
                $.each(response['value'], function(i, data) {
                    if (data['role'] == 'signatory') {
                        $userList.push(data);
                    }
                });
                console.log($userList);
            }
        });
    }

    function reuploadDocument() {
        document.getElementById('reuploadFile').value = '';
        $('#reuploadFile').trigger('click');
    }

    async function resendDocument() {
        console.log('send');
        var url = $pdfUrl;
        const existingPdfBytes = await fetch(url).then(res => res.arrayBuffer());

        // // Load a PDFDocument from the existing PDF bytes
        const pdfDoc = await PDFDocument.load(existingPdfBytes)
        const pdfBytes = await pdfDoc.saveAsBase64()
        var blob = b64toBlob(pdfBytes, 'application/pdf');
        console.log(blob);
        // receivedReject($currentPdfData['id'], $user['id'], blob).then(function(results) {
        //     console.log(results);
        // });
        reuploadReject($currentPdfData['id'], $user['id'], blob).then(function(results) {
            console.log(results);
            if (results['status'] == 'success') {
                $('#successSubmitModal').modal();
            }
        });
    }


    function requestSignature() {
        getSignature().then(function(response) {
            console.log(response);
            if (response['status'] == 'success') {
                $('#sign-list').html('');
                var signatures = response['value'];
                if (signatures.length != 0) {
                    $('#emptyState').addClass('hide');
                    $.each(signatures, function(i, data) {
                        $('#sign-list').append('<div class="pointer" data-img=' + data['signature'] + '><img class="sign-img mt-2" src="' + data['signature'] + '"></div>');
                    });
                } else {
                    $('#emptyState').removeClass('hide');
                }
            }
        });
    }

    function updateDocument(newFile) {
        updateSignedDocuments($docId, newFile).then(function(response) {
            if (response['status'] == 'success') {
                location.reload();
            }
        });
    }
    // for text image
    $("#pdf-canvas").droppable({
        drop: function(event, ui) {
            // var droppedDom = event['toElement']['id'];
            var droppedDom = event['originalEvent']['target']['id'];
            console.log(droppedDom);
            if (droppedDom == 'sampleImg') {
                $isImage = true;
                console.log('dragged sample img');
                // position of the draggable minus position of the droppable
                // relative to the document
                $imgPosX = ui.offset.left - $(this).offset().left;
                $imgPosY = ui.offset.top - $(this).offset().top;

                // startX = $newPosX;
                // startY = $newPosY;

                console.log('img x : ' + $imgPosX);
                console.log('img y : ' + $imgPosY);

                // $imgWidth = $('#sampleImg').width();
                // $imgHeight = $('#sampleImg').height();
                $imgWidth = $('#sampleImg').width();
                $imgHeight = $('#sampleImg').height();
            }

            if (droppedDom == 'imgText') {
                $isText = true;
                console.log('dragged sample text');
                // position of the draggable minus position of the droppable
                // relative to the document
                $textPosX = ui.offset.left - $(this).offset().left;
                $textPosY = ui.offset.top - $(this).offset().top;

                console.log('new x : ' + $textPosX);
                console.log('new y : ' + $textPosY);

                // $imgWidth = $('#sampleImg').width();
                // $imgHeight = $('#sampleImg').height();
                $textWidth = $('#sampleText').width() * 0.9;
                $textHeight = $('#sampleText').height() * 0.9;
            }

            // $('#sampleImg').on('mousedown', function() {
            //     $isImage = false;
            // });
            // $('#sampleImg').on('mouseup', function() {
            //     $isImage = true;
            // });
            // $('#imgText').on('mousedown', function() {
            //     $isText = false;
            // });
            // $('#imgText').on('mouseup', function() {
            //     $isText = true;
            // });
        }
    });

    // for normal image
    // $("#pdf-canvas").droppable({
    //     drop: function(event, ui) {

    //         // position of the draggable minus position of the droppable
    //         // relative to the document
    //         $textPosX = ui.offset.left - $(this).offset().left;
    //         $textPosY = ui.offset.top - $(this).offset().top;

    //         // startX = $newPosX;
    //         // startY = $newPosY;

    //         console.log('new x : ' + $textPosX);
    //         console.log('new y : ' + $textPosY);

    //         // $imgWidth = $('#sampleImg').width();
    //         // $imgHeight = $('#sampleImg').height();
    //         $imgWidth = $('#sampleText').width() * 0.9;
    //         $imgHeight = $('#sampleText').height() * 0.9;
    //         console.log('height : ' + $imgHeight);
    //     }
    // });

    const {
        degrees,
        PDFDocument,
        rgb,
        StandardFonts
    } = PDFLib


    async function modifyPdf() {

        var pngImage;
        var pngText;
        // Fetch an existing PDF document
        var url = $pdfUrl;
        const existingPdfBytes = await fetch(url).then(res => res.arrayBuffer())

        // Load a PDFDocument from the existing PDF bytes
        const pdfDoc = await PDFDocument.load(existingPdfBytes)

        // start comment
        // Fetch PNG image
        if ($isImage) {
            const pngUrl = $imgUrl;
            const pngImgUrl = $imgUrl;
            const pngImageBytes = await fetch(pngImgUrl).then((res) => res.arrayBuffer())
            pngImage = await pdfDoc.embedPng(pngImageBytes);
            const pngDims = pngImage;
            // const pngDims = pngImage.scale(1)
        }
        if ($isText) {
            const pngTextUrl = $textUrl;
            const pngTextBytes = await fetch(pngTextUrl).then((res) => res.arrayBuffer())
            pngText = await pdfDoc.embedPng(pngTextBytes);
            const pngTextDims = pngText.scale(1)
        }

        // // Get the first page of the document
        const pages = pdfDoc.getPages()
        const firstPage = pages[$currentPage - 1];

        // // Get the width and height of the first page
        const {
            width,
            height
        } = firstPage.getSize()

        // Draw the JPG image in the center of the page
        // firstPage.drawImage(jpgImage, {
        // 	x: width / 2 - jpgDims.width / 2,
        // 	y: height / 2 - jpgDims.height / 2,
        // 	width: jpgDims.width,
        // 	height: jpgDims.height,
        // })

        // Draw the PNG image near the lower right corner of the JPG image
        //from normal image
        // firstPage.drawImage(pngImage, {
        //     x: startX,
        //     y: height - (startY + 100),
        //     // y: height - (startY + 66),
        //     width: $imgWidth,
        //     // width: pngDims.width,
        //     height: $imgHeight,
        //     // height: pngDims.height,
        // })

        // text image
        if ($isImage) {
            //normal image
            firstPage.drawImage(pngImage, {
                x: $imgPosX,
                // y: height - (startY + 21),
                //pdf height - (position y + img height)
                y: height - ($imgPosY + $imgHeight),
                // y: height - $imgPosY,
                // y: height - (startY + 66),
                width: $imgWidth,
                // width: pngDims.width,
                height: $imgHeight,
                // height: pngDims.height,
            })
        }
        if ($isText) {
            firstPage.drawImage(pngText, {
                x: $textPosX,
                // y: height - (startY + 21),
                //pdf height - (position y + img height)
                y: height - ($textPosY + $textHeight),
                // y: height - $imgPosY,
                // y: height - (startY + 66),
                width: $textWidth,
                // width: pngDims.width,
                height: $textHeight,
                // height: pngDims.height,
            })
        }

        // end comment

        // // Create a string of text and measure its width and height in our custom font
        // const text = $commentText
        // const textSize = 10
        // // const textWidth = customFont.widthOfTextAtSize(text, textSize)
        // // const textHeight = customFont.heightAtSize(textSize)

        // // Draw the string of text on the page
        // firstPage.drawText(text, {
        //     x: $textPosX,
        //     y: height - ($textPosY + 17),
        //     size: textSize,
        //     // font: customFont,
        //     // color: rgb(0, 0.53, 0.71),
        //     color: rgb(0, 0, 0),
        // })

        // Serialize the PDFDocument to bytes (a Uint8Array)
        const pdfBytes = await pdfDoc.save()

        // const pdfBytes = await pdfDoc.saveAsBase64()

        // Trigger the browser to download the PDF document
        // $('#viewFile').attr('href', pdfBytes);

        // var blob = b64toBlob(pdfBytes, 'application/pdf');

        // var blob = new Blob(pdfBytes, {
        //     type: 'application/pdf'
        // });
        // var file = new File(blob, 'newname');
        // window.open(file);
        download(pdfBytes, "pdf-lib_image_embedding_example.pdf", "application/pdf");
        // updateDocument(blob);
    }

    const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, {
            type: contentType
        });
        return blob;
    }

    function showPDF(pdf_url, state) {
        console.log('show pdf');
        $("#pdf-loader").show();

        PDFJS.getDocument({
            url: pdf_url
        }).then(function(pdf_doc) {
            __PDF_DOC = pdf_doc;
            __TOTAL_PAGES = __PDF_DOC.numPages;

            $totalPages = __TOTAL_PAGES;

            $('#totalPages').html($totalPages);

            // Hide the pdf loader and show pdf container in HTML
            $("#pdf-loader").hide();
            $("#pdf-contents").show();
            $("#pdf-total-pages").text(__TOTAL_PAGES);

            // Show the first page
            showPage($currentPage, state);
        }).catch(function(error) {
            // If error re-show the upload button
            $("#pdf-loader").hide();
            $("#upload-button").show();

            alert(error.message);
        });;
    }

    function showPage(page_no, state) {
        __PAGE_RENDERING_IN_PROGRESS = 1;
        __CURRENT_PAGE = page_no;

        $('#currentPage').html(page_no);

        // Disable Prev & Next buttons while page is being loaded
        $("#pdf-next, #pdf-prev").attr('disabled', 'disabled');

        // While page is being rendered hide the canvas and show a loading message
        $("#pdf-canvas").hide();
        $("#page-loader").show();
        $("#download-image").hide();

        // Update current page in HTML
        $("#pdf-current-page").text(page_no);

        // Fetch the page
        __PDF_DOC.getPage(page_no).then(function(page) {
            // As the canvas is of a fixed width we need to set the scale of the viewport accordingly
            // var scale_required = __CANVAS.width / page.getViewport(1).width;
            var scale_required = __CANVAS.width / page.getViewport(0.5).width;

            console.log(scale_required);

            // Get viewport of the page at required scale
            var viewport = page.getViewport(1);
            console.log(viewport);
            console.log(viewport['height']);
            console.log(viewport['width']);

            $('#pageBtn').css('width', viewport['width']);

            // $('#canvas').height(845).width(595);

            //Canvas start
            if (state != 'reupload') {
                $('#pdf-canvas').css('position', 'absolute').css('z-index', '-99');
                $('#canvasPlaceholder').html('<canvas id="canvas" height="' + viewport['height'] + '" width="' + viewport['width'] + '" style="border: 1px solid black; max-width: 100%;"></canvas>');
                $('#canvas').draggable();
                $('#canvas').css('position', 'absolute');

                var canvas = document.getElementById('canvas');
                var ctx = canvas.getContext('2d');
                //Variables
                var canvasx = $(canvas).offset().left;
                var canvasy = $(canvas).offset().top;
                var last_mousex = last_mousey = 0;
                var mousex = mousey = 0;
                var mousedown = false;

                if ($isDraggable == true) {
                    $(canvas).on('mousedown', function(e) {
                        last_mousex = parseInt(e.clientX - canvasx);
                        last_mousey = parseInt(e.clientY - canvasy);

                        if ($recipientIdList.length > 0) {
                            mousedown = true;
                        } else {
                            $('#canvas').draggable({
                                disabled: true
                            });
                            mousedown = false;
                        }
                    });

                    //Mouseup
                    $(canvas).on('mouseup', function(e) {
                        if (mousedown) {
                            var answer1 = confirm('Confirm ?');
                            if (answer1) {
                                $('#annotationModal').modal();
                            }
                            mousedown = false;
                        }
                        ctx.clearRect(0, 0, canvas.width, canvas.height);
                    });

                    //Mousemove
                    $(canvas).on('mousemove', function(e) {
                        mousex = parseInt(e.clientX - canvasx);
                        mousey = parseInt(e.clientY - canvasy);
                        if (mousedown) {
                            ctx.clearRect(0, 0, canvas.width, canvas.height); //clear canvas
                            ctx.beginPath();
                            var width = mousex - last_mousex;
                            var height = mousey - last_mousey;
                            ctx.rect(last_mousex, last_mousey, width, height);
                            ctx.strokeStyle = 'black';
                            ctx.lineWidth = 1;
                            ctx.stroke();
                        }
                        //Output
                        $lastX = last_mousex + "," + canvas.width;
                        $lastY = last_mousey + "," + canvas.height;
                        $('#output').html('current: ' + mousex + ', ' + mousey + '<br/>last: ' + last_mousex + ', ' + last_mousey + '<br/>mousedown: ' + mousedown);
                    });
                }
            }


            //end canvas

            // Set canvas height
            __CANVAS.height = viewport.height;
            __CANVAS.width = viewport.width;

            var renderContext = {
                canvasContext: __CANVAS_CTX,
                viewport: viewport
            };

            // Render the page contents in the canvas
            page.render(renderContext).then(function() {
                __PAGE_RENDERING_IN_PROGRESS = 0;

                // Re-enable Prev & Next buttons
                $("#pdf-next, #pdf-prev").removeAttr('disabled');

                // Show the canvas and hide the page loader
                $("#pdf-canvas").show();
                $("#page-loader").hide();
                $("#download-image").show();
            });
        });
    }

    function changePage(direction) {
        console.log(direction);
        if (direction === 'previous') {
            if ($currentPage == 1) {
                console.log('starting page');
            } else {
                $currentPage = $currentPage - 1;
                showPage($currentPage);
            }
        } else {
            if ($currentPage == ($totalPages)) {
                console.log('end of page');
            } else {
                $currentPage = $currentPage + 1;
                showPage($currentPage);
            }
        }
    }
</script>